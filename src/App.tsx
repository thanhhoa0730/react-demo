import { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Link, Routes, useLocation, useNavigate } from 'react-router-dom';
import VocabularyList from './components/List';
import FormAdd from './components/FormAdd';
import SearchForm from './components/FormSearch';
import VocabularyListSearch from './components/ListSearch';
import vocabularyData from '../public/json/vocabulary.json';
import { Button } from 'antd';
import './App.scss';

// console.log('Vocabulary Data:', vocabularyData);

interface MainCompProps {
  vocabulary: Word[];
  addWordToVocabulary: (newWord: string, newDefinition: string) => void;
  removeWord: (indexRemove: number) => void;
}

interface Word {
  word: string;
  definition: string;
}

function App() {
  // khởi tạo state và localStorage
  // state vocabulary với giá trị ban đầu được lấy từ localStorage, nếu có dữ liệu sẽ chuyển chuỗi json
  // lưu trữ trong localstore thành mảng js. nếu k có dữ liệu, 1 mảng rỗng sẽ được sử dụng.

  const [vocabulary, setVocabulary] = useState<Word[]>(() => {
    const saveVocabulary = localStorage.getItem('vocabulary');
    if (saveVocabulary && saveVocabulary !== "[]") {
      try {
        const parsedVocabulary = JSON.parse(saveVocabulary);
        if (Array.isArray(parsedVocabulary)) {
          return parsedVocabulary;
        }
      } catch (error) {
        console.error("Parsing error: ", error);
      }
    }
    return vocabularyData;
  });

  //test gửi data trực tiếp
  // const [vocabulary, setVocabulary] = useState<Word[]>(vocabularyData);


  // console.log('State vocabulary:', vocabulary);

  // theo dõi sự thay đổi của vocabulary và cập nhật localStorage mỗi khi có từ mới được thêm hoặc xóa
  useEffect(() => {
    localStorage.setItem('vocabulary', JSON.stringify(vocabulary));
  }, [vocabulary]);

  const addWordToVocabulary = (newWord: string, newDefinition: string) => {
    // phương thức ... để tạo 1 bảng sao của mảng hiện tại và thêm từ mới vào cuối mảng
    setVocabulary((prevVocabulary: Word[]) => [...prevVocabulary, { word: newWord, definition: newDefinition }]);
  };

  const removeWord = (indexRemove: number) => {
    setVocabulary((prevVocabulary: Word[]) =>
      prevVocabulary.filter((currentItem, currentIndex) => currentIndex !== indexRemove)
    );
  };

  return (
    <Router>
      <MainComp
        vocabulary={vocabulary}
        addWordToVocabulary={addWordToVocabulary}
        removeWord={removeWord}
      />
    </Router>
  );
}

function MainComp({ vocabulary, addWordToVocabulary, removeWord }: MainCompProps) {
  const location = useLocation();

  const [searchResults, setSearchResults] = useState<Word[]>([]);

  const [hasSearched, setHasSearched] = useState(false);

  const navigate = useNavigate();

  // Hàm để điều hướng đến trang thêm từ
  const navigateToAddWord = () => {
    navigate("/add");
  };

  // Hàm để điều hướng đến trang tìm kiếm từ
  const navigateToSearchWord = () => {
    navigate("/search");
  };

  useEffect(() => {
    // Nếu đường dẫn không phải là '/search', reset kết quả tìm kiếm
    if (location.pathname !== '/search') {
      setSearchResults([]);
    }
  }, [location.pathname]); // Phụ thuộc vào đường dẫn để biết khi nào cần reset


  //isAddSearch là biến boolean xác định xem đường dẫn là add hoặc search
  const isAddSearch = location.pathname === '/add' || location.pathname === '/search';

  const handleSearchVocabulary = (searchWord: string) => {
    // console.log('Vocabulary before search:', vocabulary);
    setHasSearched(true);
    if (searchWord) {
      const results = vocabulary.filter(item =>
        item.word.toLowerCase().includes(searchWord.toLowerCase())
      );
      setSearchResults(results);
      // console.log('Search results:', results);
    } else {
      setSearchResults(vocabulary);
    }
  };

  function AddWordPage() {
    return (
      <>
        <FormAdd addWord={addWordToVocabulary} />
        <VocabularyList vocabulary={vocabulary} onRemoveWord={removeWord}/>
      </>
    );
  }

  return (
    <section>
      <div className="container">
        <h1>
          <span onClick={() => navigate("/")}>
            Vocabulary App
          </span>
        </h1>

        {!isAddSearch && (
          <div className='btn-container'>
            <Button className="btn-white" onClick={navigateToAddWord}>
              Add Word
            </Button>
            <Button className="btn-white" onClick={navigateToSearchWord}>
              Search Word
            </Button>
          </div>
        )}

        <Routes>
          <Route path="/add" element={<AddWordPage/>}/>
          <Route path="/search" element={
            <>
              <SearchForm searchVocabulary={handleSearchVocabulary}/>
              <VocabularyListSearch wordListSearch={searchResults} hasSearched={hasSearched}/>
            </>
          }/>
        </Routes>
      </div>
    </section>
  );
}

export default App;
