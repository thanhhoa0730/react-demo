//Component để nhập từ vựng mới
import React from "react";
import { useState } from "react";
import { Button, Form, Input } from 'antd';

// khai báo props
type FormProps = {
  addWord: (word: string, definition: string) => void;
}

// khai báo components
//VocabularyForm nhận prop addWord
const VocabularyForm: React.FC<FormProps> = ({addWord}) => {
  const [word, setWord] = useState('');//useState là hook của react cho phép thêm trạng thái React vào fc.
  const [definition, setDefinition] = useState('');

  const handleSubmit = () => {
    if (word && definition) {
      addWord(word, definition);
      setWord('');
      setDefinition('');
    }
  };

  return (
    <Form onFinish={handleSubmit}>
      <Form.Item
        label="Word"
        name="word"
        colon={false}
      >
        <Input
          id="word"
          value={word}
          onChange={(e) => setWord(e.target.value)}
        />
      </Form.Item>

      <Form.Item
        label="Definition"
        name="definition"
        colon={false}
      >
        <Input
          id="definition"
          value={definition}
          onChange={(e) => setDefinition(e.target.value)}
        />
      </Form.Item>

      <Button htmlType="submit" className="btn-white">
        Add Word
      </Button>
    </Form>
  );
};

export default VocabularyForm;