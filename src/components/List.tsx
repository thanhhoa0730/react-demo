//Component để hiển thị danh sách từ vựng
import React from "react";
import { Button, Table } from "antd";

// khai báo props
type ListProps = {
    vocabulary: {word: string, definition: string}[];
    onRemoveWord: (index: number) => void;
}

//VocabularyList nhận prop vocabulary
const VocabularyList: React.FC<ListProps> = ({vocabulary, onRemoveWord}) => {
    const columns = [
        {
          title: 'Word',
          dataIndex: 'word',
          key: 'word',
        },
        {
          title: 'Definition',
          dataIndex: 'definition',
          key: 'definition',
        },
        {
          title: 'Action',
          key: 'action',
          render: (_:unknown, record: { word: string; definition: string }, index: number) => { // Hàm render cho cột này
            // Tạo một Button với sự kiện onClick để gọi hàm onRemoveWord với index
            return (
              <Button type="dashed" danger onClick={() => onRemoveWord(index)}>
                Delete
              </Button>
            );
          },
        },
    ];

    const isVocabularyEmpty = vocabulary.length === 0;

    const content = isVocabularyEmpty ? (
    // Nếu từ vựng rỗng, hiển thị thông báo không tìm thấy từ vựng
    <p>Not found vocabulary</p>
    ) : (
    // Nếu có từ vựng, sử dụng Table từ Ant Design để hiển thị
    <Table
        dataSource={vocabulary}
        columns={columns}
        pagination={false}
        rowKey={record => `${record.word}-${record.definition}`}/>
    );

    // Component sẽ trả về biến content
    return <div className="rounded-table">{content}</div>;
};

export default VocabularyList;
