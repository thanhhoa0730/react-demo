//Component để nhập từ vựng mới
import React from "react";
import { useState } from "react";
import { Button, Form, Input } from 'antd';

// khai báo props
type FormProps = {
  searchVocabulary: (word: string) => void;
}

// khai báo components
//SearchForm nhận prop addWord
const SearchForm: React.FC<FormProps> = ({searchVocabulary}) => {
    const [searchWord, setSearchWord] = useState('');

    const handleSearchSubmit = () => {
      searchVocabulary(searchWord);
    };

  return (
    <Form onFinish={handleSearchSubmit}>
      <Form.Item
        label="Search Word"
        name="searchWord"
        colon={false}
        rules={[
          {
            required: true,
            message: 'Please input your search word!',
          }
        ]}
      >
        <Input
          id="searchWord"
          value={searchWord}
          onChange={(e) => setSearchWord(e.target.value)}
        />
      </Form.Item>

      <Button htmlType="submit" className="btn-white">
        Search
      </Button>
    </Form>
  );
};

export default SearchForm;
