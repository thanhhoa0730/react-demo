// Component này sẽ lắng nghe searchResults và render chúng ra màn hình
import React from "react";
import { Table } from "antd";

// khai báo props
type ListSearchProps = {
    wordListSearch: {word: string, definition: string}[];
}

//VocabularyList nhận prop vocabulary
const VocabularyListSearch: React.FC<ListSearchProps & { hasSearched: boolean }> = ({wordListSearch, hasSearched}) => {
    if (!hasSearched) {
        return null;
    }
    if (wordListSearch.length === 0) {
        return <p className="lead">No search results found.</p>;
    }

    const columns = [
        {
            title: 'Word',
            dataIndex: 'word',
            key: 'word',
            render: (text: string) => <strong>{text}</strong>,
        },
        {
          title: 'Definition',
          dataIndex: 'definition',
          key: 'definition',
        }
    ];
    return (
        <Table className="rounded-table" dataSource={wordListSearch} columns={columns} pagination={false}/>
    );

};

export default VocabularyListSearch;
